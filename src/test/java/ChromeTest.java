import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class ChromeTest {
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp () {
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
    }

    @BeforeMethod
    public void testsSetUp () {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/search");
    }

    @org.testng.annotations.Test
    public void checkSearchingImage(){
        driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("image");
        driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).click();
        driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys(Keys.ENTER);
        List<WebElement> elementsList = driver.findElements(By.xpath("//img[@alt='Картинки по запросу image']"));
        int actualElementsSize=elementsList.size();
        assertTrue(actualElementsSize>0);

    }

    @AfterMethod
    public void tearDown () {
        driver.quit();
    }
}
